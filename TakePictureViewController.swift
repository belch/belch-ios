
//
//  TakePictureViewController.swift
//  Photos Gallery App
//
//  Created by Nitesh on 12/6/14.
//
import UIKit
import QuartzCore
import CoreImage


class TakePictureViewController: UIViewController, UIImagePickerControllerDelegate, UIDocumentInteractionControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate {
    
    var userPizza : Int!
    
    var rating = ["1", "2","3","4"]
    
    @IBOutlet weak var drawingView: UIView!
    @IBOutlet weak var drawingImageView: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var blurContainerView: UIView!
    
    @IBOutlet weak var burgers: UIImageView!
    @IBOutlet weak var transparentView: UIView!
    
    
    @IBOutlet weak var largeImage: UIImageView!
    
    
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var dishname_tf: UITextField!
    
    @IBOutlet weak var addReviewOutlet: UIButton!
    
    @IBOutlet weak var dollar_tf: UITextField!
    @IBOutlet weak var reviewDetailsView: UIView!
    
    @IBOutlet weak var dispRating: UILabel!
    @IBOutlet weak var ratinglabel: UILabel!
    
    @IBOutlet weak var post: UIButton!
    
    
    @IBOutlet var bigView: UIView!

    
    @IBAction func postbutton(sender: AnyObject) {
        
       /* var imageData = UIImagePNGRepresentation(image)
        
        //OR with path
        var url:NSURL = NSURL.URLWithString("urlHere")
        var imageData:NSData = NSData.dataWithContentsOfURL(url, options: nil, error: nil)
*/
        
        if(dispRating.text != "" && dollar_tf.text != "" && imageView.image != nil && dishname_tf.text != ""){
    
        var imageData = UIImageJPEGRepresentation(imageView.image, 1.0)
        //imageView.image

        let base64String = imageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.allZeros)
        
        NSLog("this is the string//////// " + base64String)
        
        let rating = dispRating.text?.toInt()
        let price = dollar_tf.text?.toInt()
        let item = dishname_tf.text
    
        
        postReview(base64String, rating: rating!, price: price!, item: item)
        
        
     //   RestaurantViewController.tableView(reloadInputViews())
       // RestaurantViewController.loadRestaurantView(<#RestaurantViewController#>)
        
       // RestaurantViewController.reloadInputViews(<#UIResponder#>)
        
        
        NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
        
        self.navigationController?.popViewControllerAnimated(true)
        }
        
  
        
        
    }
    
    // Method that invokes the post to review
    // Need to grab: Image URL, the rating, price and what item the user entered
    // Also grab the pizza from the intial pizza view
    func postReview(image: String, rating: Int, price: Int, item: String){
        
        var review = Review(pizza: self.userPizza , image: image, rating: rating, price: price, item: item)
        
        self.postService.makePost(self.restaurant.getId(), review: review)
        
        //dispRatin
        
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return rating.count
         
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String!{
        return rating[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        var itemSelected = rating[row]
        dispRating.text = itemSelected
       // var p3: CGPoint = CGPoint(135,340)
        
        var img: UIImage! = UIImage(named:"burger_1.png")
         var img2: UIImage! = UIImage(named:"burger_2.png")
         var img3: UIImage! = UIImage(named:"burger_3.png")
         var img4: UIImage! = UIImage(named:"burger_4.png")
        
        burgers.alpha = 0.0
        
        if(dispRating.text == "0"){
            burgers.image = nil
        }else if(dispRating.text == "1"){
            burgers.image = img
        }else if(dispRating.text == "2"){
            burgers.image = img2
        }else if(dispRating.text == "3"){
            burgers.image = img3
        }else if(dispRating.text == "4"){
            burgers.image = img4
        }
    }
    
    @IBAction func delicious(sender: AnyObject) {
        UIImageView.animateWithDuration(0.7, delay: 0, options: .TransitionNone, animations: {
            self.blurContainerView.alpha = 0.0
            self.imageView.alpha = 1.0
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            self.navigationController?.setToolbarHidden(false, animated: false)
            
            }, completion: { finished in
                println("UNBLURRRRRED")
                
        })
    }
    
    @IBAction func addReview(sender: AnyObject) {
        
        imageView.alpha = 0.0
        
        addReviewOutlet.alpha = 0.0
       // post.alpha = 0.0
        
      //  dishname_tf.tintColor = UIColor.blueColor()
      //  dollar_tf.tintColor = UIColor.blueColor()
        dishname_tf.tintColor =  UIColorFromRGB(0x1ABC9C)
        dollar_tf.tintColor =  UIColorFromRGB(0x1ABC9C)
        
        UIView.animateWithDuration(0.7, delay: 0, options:.TransitionFlipFromLeft, animations: {
            /*self.blurContainerView.alpha = 1.0
            self.imageView.alpha = 0.0
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController?.setToolbarHidden(true, animated: false)*/
            self.reviewDetailsView.alpha = 1.0
            
            }, completion: { finished in
                println("review Details")
                
        })
        
        
    }

    
    @IBOutlet weak var done: UIButton!
    
    
    @IBAction func done(sender: AnyObject) {
        
        if(dishname_tf.text != "" && dollar_tf.text != ""){
        picker.alpha = 0.0
        dissmissDetails()
        done.alpha = 0.0
        burgers.alpha = 0.0
       // dispRating.alpha = 0.0
       // post.alpha = 1.0
        //blurContainerView.insertSubview(newView, belowSubview: transparentView)
        dishname_tf.userInteractionEnabled = false
        dollar_tf.userInteractionEnabled = false
        
        }
            
        

        
    }

    
    @IBAction func keyboardShown(sender: AnyObject) {
         NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardDidShowNotification, object: nil)
        
        
    }
    
    
    let dic: UIDocumentInteractionController = UIDocumentInteractionController()
    
    var postService : RestaurantService!
    var restaurant : Restaurant!
    
    override func viewWillAppear(animated: Bool) {
        blurContainerView.alpha = 0.0
    }
    
    func blur(recognizer: UITapGestureRecognizer){
         self.largeImage.image = self.imageView.image
        captureBlur()
       
        
        UIImageView.animateWithDuration(0.7, delay: 0, options: .TransitionNone, animations: {
          self.blurContainerView.alpha = 1.0
            self.imageView.alpha = 0.0
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController?.setToolbarHidden(true, animated: false)
            
            }, completion: { finished in
                println("BLURRRRRED")
                
        })
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    override func supportedInterfaceOrientations() -> Int {
       return UIInterfaceOrientation.Portrait.rawValue
    }
    
    
    func captureBlur(){
        NSLog("blurring")
        UIGraphicsBeginImageContext(largeImage.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext())
        var viewImage: UIImage =  UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsGetImageFromCurrentImageContext()
        
        var num: NSNumber = NSNumber(float: 20)
        
        var imageToBlur: CIImage = CIImage(image: viewImage)

        
        
       // var imageToBlur: CIImage =
        var gaussianBlurFilter: CIFilter = CIFilter(name: "CIGaussianBlur")
        gaussianBlurFilter.setValue(imageToBlur, forKey: "inputImage")
        gaussianBlurFilter.setValue(num, forKey: "inputRadius")
        var resultImage: CIImage = gaussianBlurFilter.valueForKey("outputImage") as CIImage
        
        var blurredImage: UIImage! = UIImage(CIImage: resultImage)
       
        var newView: UIImageView = UIImageView(frame: self.view.bounds)
        newView.image = blurredImage
        blurContainerView.insertSubview(newView, belowSubview: transparentView)
        
        
    }
    
    @IBAction func dismiss_keyboard(sender: AnyObject) {
        
        sender.resignFirstResponder()

        
    }
    
    
    
    
    func dissmissDetails(){
            
            let screenRect: CGRect = UIScreen.mainScreen().bounds
            var screenHeight: CGFloat = CGFloat()
            var heightOfScreen: Int = Int(screenHeight)
            
            var frame1: CGRect = dishname_tf.frame;
            var frame2: CGRect = dollar_tf.frame;
        var frame3: CGRect = burgers.frame
        var frame4: CGRect = dispRating.frame
        var frame5: CGRect = ratinglabel.frame
        
            frame1.origin.y = 348
            
            frame2.origin.y = 348
        
            frame3.origin.y = 348
        
            frame4.origin.y = 398
        
            frame5.origin.y = 398
        
        
            UIImageView.animateWithDuration(0.7, delay: 0, options: .TransitionNone, animations: {
            self.imageView.alpha = 1.0
            self.dishname_tf.frame = frame1
            self.dollar_tf.frame = frame2
            self.burgers.frame = frame3
            self.dispRating.frame = frame4
            self.ratinglabel.frame = frame5
            
            }, completion: { finished in
            println("KeyboardNotShowen")
            
            })
            
            
            
    }
    
    func keyboardWasShown(notification:NSNotification){
        let info: NSDictionary = notification.userInfo!
        let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as NSValue
        let keyboardSize: CGSize = value.CGRectValue().size
        
        let keyboardHeight = keyboardSize.height
        let screenRect: CGRect = UIScreen.mainScreen().bounds
        
        
        
        let screenHeight: CGFloat = screenRect.size.height
        let heightWithKeyboardShown: CGFloat = screenHeight - keyboardHeight
        
        //  var modHeight: Int = Int(heightWithKeyboardShown)
        
        var frame1: CGRect = dishname_tf.frame;
        var frame2: CGRect = dollar_tf.frame;
        frame1.origin.y = (heightWithKeyboardShown * 50)/100
        frame2.origin.y = (heightWithKeyboardShown * 50)/100
        
        UIImageView.animateWithDuration(0.7, delay: 0, options: .TransitionNone, animations: {
        self.imageView.alpha = 0.0
        self.dishname_tf.frame = frame1
        self.dollar_tf.frame = frame2
        
        }, completion: { finished in
        println("keyboardShown")
        
        })
        
        
        
        
        
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "belch_logo.png")
        let restaurantLogo = UIImageView(image: logo)
        //  restaurantLogo.set
        self.navigationItem.titleView = restaurantLogo
        dispRating.text = rating[0]
        
        reviewDetailsView.alpha = 0.0
       // post.alpha = 0.0
        
      /*
      NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardDidShowNotification, object: nil)
        */

        
        
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        
        if(imageView.image == nil){
        imageView.backgroundColor = UIColor.whiteColor()
        }
        imageView.userInteractionEnabled = true
        
        let recognizer = UITapGestureRecognizer(target: self, action:Selector("blur:"))
        
        imageView.addGestureRecognizer(recognizer)
        
        
        
        self.largeImage.layer.shadowColor = UIColor.blackColor().CGColor
        self.largeImage.layer.shadowOffset = CGSizeMake(2.0, 2.0)
        self.largeImage.layer.shadowOpacity = 0.41
        self.largeImage.layer.shadowRadius = 5.0
        postService = RestaurantService()
        
        var im: UIImage = UIImage(named: "instagrampic.png")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        
        var inst: UIBarButtonItem = UIBarButtonItem(image: im, landscapeImagePhone: im, style: UIBarButtonItemStyle.Plain, target: self, action:nil)
        
        func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
        )
        }
        
        self.view.backgroundColor = UIColorFromRGB(0xECF0F1)
    }
    

    
    @IBAction func cameraButton(sender: AnyObject) {
        var picker: UIImagePickerController = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.Camera
        picker.allowsEditing = true
        self.presentViewController(picker, animated: true, completion: nil)
        
    }
    
    @IBAction func Share(sender: AnyObject) {
        
        if(self.imageView.image != nil){
            
        
        
        var screenShot: UIImage = self.imageView.image!
        var savePath: NSString = NSHomeDirectory().stringByAppendingPathComponent("Documents/Test.ig")
        UIImagePNGRepresentation(screenShot).writeToFile(savePath, atomically: true)
        
        var rect: CGRect = CGRectMake(0, 0, 0, 0)
        var jpgPath: NSString = NSHomeDirectory().stringByAppendingPathComponent("Documents/Test.ig")
        var igImageHookFile: NSURL = NSURL(string: NSString(format: "file://%@", jpgPath))!
        self.dic.UTI = "com.instagram.photo"
        // self.dic = self.setupControllerWithURL(igImageHookFile, interactionDelegate: self)
        dic.URL = igImageHookFile
        //dic = self.setupControllerWithURL(igImageHookFile, interactionDelegate: self)
        
        self.dic.presentOpenInMenuFromRect(rect, inView: self.view, animated: true)
        var instagramURL: NSURL! = NSURL(string: "instagram://media?id=MEDIA_ID")
        if(UIApplication.sharedApplication().canOpenURL(instagramURL)){
            self.dic.presentOpenInMenuFromRect(rect, inView: self.view, animated: true)
        }else{
            NSLog("No Instagram Found")
        }
        }
    }
    
    @IBAction func SearchPhotosButton(sender: AnyObject) {
        var imagePicker : UIImagePickerController = UIImagePickerController()
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func thumbnailFromView(myView: UIImage) -> UIImage{
        //Int size = myView.frame.size
        // UIImag
        
        // return self.thumbnailFromView(myView, withSize: myView.frame.size)
        //   return self.resizeImage( , newSize: myView.frame.size)
        return self.resizeImage(myView, newSize: myView.size)
        
    }
    
    func resizeImage(image: UIImage, newSize: CGSize) -> (UIImage) {
        let newRect = CGRectIntegral(CGRectMake(0,0, newSize.width * 2, newSize.height * 2))
        let imageRef = image.CGImage
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        // Set the quality level to use when rescaling
        CGContextSetInterpolationQuality(context, kCGInterpolationHigh)
        let flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height)
        
        CGContextConcatCTM(context, flipVertical)
        // Draw into the context; this scales the image
        CGContextDrawImage(context, newRect, imageRef)
        
        let newImageRef = CGBitmapContextCreateImage(context) as CGImage
        let newImage = UIImage(CGImage: newImageRef)
        
        // Get the resized image from the context and a UIImage
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info: NSDictionary!){
            self.dismissViewControllerAnimated(true, completion: nil)
            imageView.image = info.valueForKey(UIImagePickerControllerEditedImage) as UIImage
            
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController!){
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    

}
