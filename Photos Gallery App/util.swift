//
//  util.swift
//  Photos Gallery App
//
//  Created by Chung, Brian on 12/6/14.
//  Copyright (c) 2014 Abbouds Corner. All rights reserved.
//

import Foundation

func UIColorFromRGB(rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}