//
//  ViewController.swift
//  BelchApp
//
//  Created by Matthew Hogetvedt on 10/21/14.
//  Copyright (c) 2014 Belch. All rights reserved.
//

import UIKit

class RestaurantViewController: GAITrackedViewController, UITableViewDataSource {
    
    var restaurantId = "carnitas-snack-shack-san-diego" // TODO this gets passed in from the restaurant list
    
    var service: RestaurantService!
    var imgFactory: ImageFactory!
    
    var restaurant: Restaurant! // INCOMING restaurant model
    var reviews: [Review]!      // OUTGOING review model


    @IBOutlet var restaurantName: UILabel!
    @IBOutlet var restaurantAddress: UILabel!
    @IBOutlet weak var restaurantMainImg: UIImageView!
    @IBOutlet weak var restaurantRatingImg: UIImageView!
    @IBOutlet weak var reviewTableView: UITableView!

    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var userPizza : Int!
    var cellId = "RCell"    // ID for the cell prototype in RestaurantVC
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadList:",name:"load", object: nil)

        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(false , animated: false)
        
        let logo = UIImage(named: "belch_logo.png")
        let restaurantLogo = UIImageView(image: logo)
        //  restaurantLogo.set
        self.navigationItem.titleView = restaurantLogo
        //    self.navigationItem.
        
        reviews = [Review]()
        service = RestaurantService()
        imgFactory = ImageFactory()
        hideView()
        loadRestaurantView()
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadList:",name:"load", object: nil)
      //  self.reviewTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func loadList(notification: NSNotification){
        //load data here
        self.reviewTableView.reloadData()
    }
    
    // loads all data onto the view
    func loadRestaurantView(){
        service.getRestaurant(self.restaurant.getId(),  {
            (response) in
            self.restaurant.loadAddressFromNSArray(response["address"]! as NSArray)
            self.restaurant.loadReviewsFromNSArray(response["reviews"]! as NSArray, userPizza: self.userPizza)
            
            // load data into view asynchronously
            dispatch_async(dispatch_get_main_queue()){
                
                
                self.reviews = self.restaurant.getReviews()
                self.loadViewData()
                self.reviewTableView.reloadData()
                self.showView()
            }
        })
    }
    
    // this method does all loading of data INTO the view its self
    func loadViewData(){
        loadMainImg()
        
        restaurantRatingImg.image = self.restaurant.getBelchRatingImg()
        
        restaurantName.text = self.restaurant.getName()
        
        restaurantAddress.text = self.restaurant.getAddressAsString()
    }
    
    // pulls a random image from a review, then sets it as the main image in restaurantVC
    func loadMainImg(){
        if(self.restaurant.getReviews().count != 0){
            var numImgs = self.restaurant.getReviews().count
            let i = UInt32(numImgs)
            let random = Int(arc4random_uniform(i))
            var review = self.restaurant.getReviews()[random]
        
            self.restaurantMainImg.image = self.imgFactory.createUIImageFromString(review.getImage())
        } else {
            self.restaurantMainImg.image = self.imgFactory.createUIImageFromString(self.restaurant.getImage())
        }
    }

    // number of cells based on review count
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }
    
    // populates the review table
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as ReviewCell
        
        var review = self.reviews[indexPath.row] as Review
        cell.reviewCellTitle.text = review.getItem()
        cell.reviewPrice.text = "$" + String(review.getPrice())
        
        if(cell.reviewCellImg.image == nil){
            cell.reviewCellRatingImg.image = review.getBelchRatingImg()
            if(review.getImage() == ""){
                // NO IMAGE AVAILABLE: add default/missing image here
                cell.reviewCellImg.image = UIImage(named: "no_image.jpg")
            }else {
                cell.reviewCellImg.image = imgFactory.createUIImageFromString(review.getImage())
            }
        }
        return cell
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName = "Restaurant View"
    }
    
    func hideView(){
        self.activityIndicator.hidden = false
        self.activityIndicator.startAnimating()
        self.restaurantName.hidden = true
        self.restaurantAddress.hidden = true
        self.restaurantMainImg.hidden = true
        self.restaurantRatingImg.hidden = true
        self.reviewTableView.hidden = true
    }
    
    func showView(){
        self.activityIndicator.hidden = true
        self.activityIndicator.stopAnimating()
        self.restaurantName.hidden = false
        self.restaurantAddress.hidden = false
        self.restaurantMainImg.hidden = false
        self.restaurantRatingImg.hidden = false
        self.reviewTableView.hidden = false
    }
    
    // transition for the ReviewViewController
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "restaurantToReviewSegue"){
            let row = self.reviewTableView.indexPathForSelectedRow()?.row
            var review = self.reviews[row!] as Review
            var svc = segue.destinationViewController as ReviewViewController

            svc.review = review
        } else if (segue.identifier == "createReviewSegue"){
            // do stuff that transitions to the review controller (if requried)
            let svc = segue.destinationViewController as TakePictureViewController
            svc.userPizza = self.userPizza
            svc.restaurant = self.restaurant
        }
    }
}

