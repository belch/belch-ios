//
//  ReviewCell.swift
//  BelchApp
//
//  Created by Matthew Hogetvedt on 11/29/14.
//  Copyright (c) 2014 Belch. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {
    
    var imgFactory : ImageFactory!
    var isUsed = false
    
    @IBOutlet weak var reviewCellImg: UIImageView!
    @IBOutlet weak var reviewCellTitle: UILabel!
    @IBOutlet weak var reviewCellRating: UILabel!
    @IBOutlet weak var reviewCellRatingImg: UIImageView!
    @IBOutlet weak var reviewPrice: UILabel!
    
    
    
    func setCell(image: String, item: String, rating: Int, ratingImg: UIImage){
        
        self.reviewCellTitle.text = item
        self.reviewCellRating.text = String(rating)
        self.reviewCellRatingImg.image = ratingImg
        self.reviewCellImg.image = self.imgFactory.createUIImageFromString(image)
    }

}
