//
//  Review.swift
//  BelchApp
//
//  Created by Matthew Hogetvedt on 10/21/14.
//  Copyright (c) 2014 Belch. All rights reserved.
//

import Foundation


class Review {

    var pizza : Int
    var image : String
    var rating : Int
    var price : Int
    var item : String
    var belchRatingImg : UIImage

    init(pizza : Int, image : String, rating : Int, price : Int, item : String){
        self.pizza = pizza
        self.image = image
        self.rating = rating
        self.price = price
        self.item = item
        var belch : Float = Float(rating) / Float(price)
        var avgBelch : Int = Int(round(belch))
        if avgBelch == 1 {
            self.belchRatingImg = UIImage(named: "burger_1.png")!
        } else if avgBelch == 2 {
            self.belchRatingImg = UIImage(named: "burger_2.png")!
        } else if avgBelch == 3 {
            self.belchRatingImg = UIImage(named: "burger_3.png")!
        } else {
            self.belchRatingImg = UIImage(named: "burger_4.png")!
        }
    }
    
    func getBelchRatingImg() -> UIImage {
        return self.belchRatingImg
    }
    
    func getPizza() -> Int{
        return self.pizza
    }
    
    // TODO need to correctly store and return picture. Is it a string? Blob? Byte array?
    func getImage() -> String {
        return self.image
    }
    
    func getRating() -> Int {
        return self.rating
    }
    
    func getPrice() -> Int {
        return self.price
    }
    
    func getItem() -> String {
        return self.item
    }

    
    
    
}


//"pizza":2,
//"picture":"wjkandkawjndajdknawd",
//"rating":2,
//"price":3,
//"item":"burger",