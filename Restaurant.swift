//
//  Restaurant.swift
//  BelchApp
//
//  Created by Matthew Hogetvedt on 10/21/14.
//  Copyright (c) 2014 Belch. All rights reserved.
//

import Foundation

class Restaurant{
    
    var id : String
    var name : String
    var phone : String
    var image : String
    var rating : Int
    var ratingImg : String
    var belchRatingImg : UIImage
    
    var address =  [String]()
    var reviews = [Review]()

    init(id: String, name : String, phone : String, image : String, rating: Int, ratingImg: String, belchRatingImg: UIImage/*, address : NSArray, reviews : NSArray*/){
        self.id = id
        self.name = name
        self.phone = phone
        self.image = image
        self.belchRatingImg = belchRatingImg
        self.rating = rating
        self.ratingImg = ratingImg
    }
    
    func setBelchRatingImg(belchRatingImg: UIImage) {
        self.belchRatingImg = belchRatingImg
    }
    
    func getBelchRatingImg()->UIImage {
        return self.belchRatingImg
    }
    
    func setRating(rating: Int){
        self.rating = rating
    }
    
    func getRating()-> Int{
        return self.rating
    }
    
    func setRatingImg(ratingImg: String){
        self.ratingImg = ratingImg
    }
    
    func getRatingImg() -> String{
        return self.ratingImg
    }
    
    
    func getId() -> String{
        return self.id
    }
    
    func setId(id : String){
        self.id = id
    }
    
    func getName() -> String{
        return self.name
    }
    
    func getPhone() -> String{
        return self.phone
    }
    
//    TRUE if closed, FALSE if open
//    func getClosed() -> Bool {
//        return self.closed
//    }
  
    func getImage() -> String {
        return self.image
    }
    
    func addReview(review : Review){
        self.reviews.append(review)
    }
    
    // TEST ME, is this the correct way to return an array?
    func getReviews() -> [Review]{
        return self.reviews
    }
    
    func loadReviewsFromNSArray(reviews: NSArray, userPizza: Int){
        self.reviews = [Review]()
        for review in reviews {
            var pizza = review["pizza"] as Int
            if(pizza == userPizza){
                var image = review["picture"] as String // TODO: Needs to proparly store picture
                var rating = review["rating"] as Int
                var price = review["price"] as Int
                var item = review["item"]! as String
            
                var tempReview = Review(pizza : pizza,
                    image: image,
                    rating: rating,
                    price: price,
                    item: item)
            
                self.addReview(tempReview)
            }
        }
    }
    
    func addAddress(address : String){
        self.address.append(address)
    }
    
    func getAddressAsList() -> [String]{
        return self.address
    }
    
    func getAddressAsString() -> String{
        var temp : String = ""
        
        for i in self.address{
            temp = temp + i + " "
        }
        return temp
    }
    
    func loadAddressFromNSArray(address: NSArray){
        for i in address {
            self.addAddress((i as String))
        }
    }
    
}