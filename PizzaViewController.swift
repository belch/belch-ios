//
//  PizzaViewController.swift
//  Photos Gallery App
//
//  Created by Nitesh on 11/20/14.
// THIS IS THE REAL ONE.

import UIKit


class PizzaViewController: GAITrackedViewController {

    @IBOutlet weak var TL: UIImageView!
    @IBOutlet weak var TR: UIImageView!
    
    @IBOutlet weak var BL: UIImageView!
    
    @IBOutlet weak var BR: UIImageView!
    
    @IBOutlet weak var sliceText: UILabel!
    
    @IBOutlet weak var continueBtn: UIBarButtonItem!
    
    @IBOutlet weak var pizzaBG: UIImageView!
    
    @IBOutlet weak var bg_card: UIImageView!
    
    @IBOutlet weak var plateImageView: UIImageView!
    
    var count  = 0
    var count1 = 0
    var count2 = 0
    var count3 = 0
    var slices = 0
    
   
    
    
    
    /*var frameTL: CGRect!
    var frameTR: CGRect!
    var frameBR: CGRect!
    var frameBL: CGRect!
    */
    
    
    
    
    
    
    
    func dispSlices(){
        
        slices = count + count1 + count2 + count3;
        checkSlices()
        /*
        if(slices == 1){
        sliceText.text = "1 slice? My grandma can eat more than that."
        }else if(slices == 2){
        sliceText.text = "2 slices? Pretty Average, Pretty standard."
        }else if(slices == 3){
        sliceText.text = "3 slices? You are almost a man."
        }else if(slices == 4){
        sliceText.text = "You're a BEAST!"
        }*/
        
        
        // self.sliceText.alpha = 1.0
        /* UIView.animateWithDuration(2.0, animations:{ ()-> Void in self.sliceText.alpha = 1.0
        }) */
        
        
        //        UILabel.animateWithDuration(0.7, delay: 0, options: .TransitionFlipFromLeft, animations: {
        //           //var frameTL = self.TL.frame
        //
        //           // self.sliceText.alpha = 1.0
        //            //self.sliceText.text = "It Worked"
        //
        //
        //           // self.TL.frame = frameTL
        //
        //            }, completion: { finished in
        //                println("IT WORKED")
        //
        //        })
    }
    
    func handleTap(recognizer: UITapGestureRecognizer) {
        
        if(count == 0){
            UIImageView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
                var frameTL = self.TL.frame
                frameTL.origin.y -= frameTL.size.height/16
                frameTL.origin.x -= frameTL.size.width/16
                
                
                
                self.TL.frame = frameTL
                
                }, completion: { finished in
                    println("TL GO OUT")
                    
            })
           
            count++
            dispSlices()
            
            
        }else{
            UIImageView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
                var frameTL = self.TL.frame
                println(frameTL.origin.x)
                println(frameTL.origin.y)
                frameTL.origin.y += frameTL.size.height/16
                frameTL.origin.x += frameTL.size.width/16
                
                
                self.TL.frame = frameTL
                
                }, completion: { finished in
                    println("TL GO IN")
                    
            })
            
            count--
            dispSlices()
        }
        
        
    }
    
    func handleTap1(recognizer: UITapGestureRecognizer) {
        
        if(count1 == 0){
            UIImageView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
                var frameTR = self.TR.frame
                frameTR.origin.y -= frameTR.size.height/16
                frameTR.origin.x += frameTR.size.width/16
                
                self.TR.frame = frameTR
                
                }, completion: { finished in
                    println("TR GO OUT")
                    
            })
            count1++
            dispSlices()
        }else{
            
            UIImageView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
                var frameTR = self.TR.frame
                frameTR.origin.y += frameTR.size.height/16
                frameTR.origin.x -= frameTR.size.width/16
                
                self.TR.frame = frameTR
                
                }, completion: { finished in
                    println("TR GO IN")
                    
            })
            
            count1--
            dispSlices()
        }
        
        
    }
    func handleTap2(recognizer: UITapGestureRecognizer) {
        
        if(count2 == 0){
            UIImageView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
                var frameBR = self.BR.frame
                frameBR.origin.y += frameBR.size.height/16
                frameBR.origin.x += frameBR.size.width/16
                
                
                self.BR.frame = frameBR
                
                }, completion: { finished in
                    println("BR GO OUT")
                    
            })
            count2++
            dispSlices()
        } else{
            
            UIImageView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
                var frameBR = self.BR.frame
                frameBR.origin.y -= frameBR.size.height/16
                frameBR.origin.x -= frameBR.size.width/16
                
                
                self.BR.frame = frameBR
                
                }, completion: { finished in
                    println("BR GO IN")
                    
            })
            
            count2--
            dispSlices()
        }
        
        
    }
    func handleTap3(recognizer: UITapGestureRecognizer) {
        
        // println("handle tap3 is working")
        if(count3 == 0){
            UIImageView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
                var frameBL = self.BL.frame
                frameBL.origin.y += frameBL.size.height/16
                frameBL.origin.x -= frameBL.size.width/16
                self.BL.frame = frameBL
                
                }, completion: { finished in
                    println("BL GO OUT")
                    
            })
            count3++
            dispSlices()
        }else{
            
            UIImageView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
                var frameBL = self.BL.frame
                frameBL.origin.y -= frameBL.size.height/16
                frameBL.origin.x += frameBL.size.width/16
                self.BL.frame = frameBL
                
                }, completion: { finished in
                    println("BL GO IN")
                    
            })
            
            count3--
            dispSlices()
        }
        
        
    }
    
    
    
    
    /*  override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
    // dispSlices()
    }*/
    
    func checkSlices(){
        
        if(slices == 0){
            self.continueBtn.enabled = false
        }else{
            self.continueBtn.enabled = true
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sliceText.lineBreakMode  = .ByWordWrapping
        sliceText.numberOfLines = 0
        sliceText.text = "Tap On Pizza And Tell Us Your Appetite!"
        
        ///////////////////////
        
        let logo = UIImage(named: "belch_logo.png")
        let restaurantLogo = UIImageView(image: logo)
        //  restaurantLogo.set
        self.navigationItem.titleView = restaurantLogo
        //service = RestaurantService()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        ////////////////////////
        
        //self.plateImageView.
        self.pizzaBG.backgroundColor = UIColorFromRGB(0xECF0F1)
        
        checkSlices()

        self.view.backgroundColor = UIColorFromRGB(0xECF0F1)
        self.navigationController?.toolbar.barTintColor = UIColorFromRGB(0x16A085)
        
        TL.userInteractionEnabled = true
        TR.userInteractionEnabled = true
        BR.userInteractionEnabled = true
        BL.userInteractionEnabled = true
        
        
        let recognizer = UITapGestureRecognizer(target: self, action:Selector("handleTap:"))
        let recognizer1 = UITapGestureRecognizer(target: self, action:Selector("handleTap1:"))
        let recognizer2 = UITapGestureRecognizer(target: self, action:Selector("handleTap2:"))
        let recognizer3 = UITapGestureRecognizer(target: self, action:Selector("handleTap3:"))
        
        // let labelRec = UITapGestureRecognizer(target: self, action:Selector("dispSlices:"))
        
        //dispSlices()
        
        TL.addGestureRecognizer(recognizer)
        TR.addGestureRecognizer(recognizer1)
        BR.addGestureRecognizer(recognizer2)
        BL.addGestureRecognizer(recognizer3)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientation.Portrait.rawValue
    }
    
    
    
    override func viewDidAppear(animated: Bool) {
       // self.navigationController?.setNavigationBarHidden(true, animated: false)
       // self.navigationController?.setToolbarHidden(true, animated: false)
      //  self.navigationController?.navigationBarHidden = true
     //   self.navigationController?.setToolbarHidden(true, animated: false)
       // self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.blueColor()]
        

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName = "Pizza Screen"
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Create a new variable to store the instance of PlayerTableViewController
        let destinationVC = segue.destinationViewController as SearchViewController
        destinationVC.slicesPassedIn = self.slices
    }
    
    
}

