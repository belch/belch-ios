//
//  RestaurantService.swift
//  BelchApp
//
//  Created by Matthew Hogetvedt on 11/10/14.
//  Copyright (c) 2014 Belch. All rights reserved.
//

import Foundation

class RestaurantService {
    var settings: Settings!
    
    init(){
        self.settings = Settings()
    }
    
    // grabs a specific restaurant
    func getRestaurant(restaurantId : String, callback: (NSDictionary) -> ()){
        request(settings.getRestaurant(restaurantId), callback: callback)
    }
    
    // posts a review with given restaurant ID
    func makePost(restaurantId : String, review : Review){
        post(settings.getRestaurant(restaurantId), pizza : review.getPizza(), picture : review.getImage(), rating: review.getRating(), price: review.getPrice(), item: review.getItem())
    }
    

    private func request(url: String, callback: (NSDictionary) -> ()){
        var nsURL = NSURL(string: url)
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(nsURL!) {
            (data, response, error) in
            var error: NSError?
            var response = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &error) as NSDictionary
            callback(response)
        }
        task.resume()
    }
    
    private func post(url: String, pizza : Int, picture : String, rating : Int, price : Int, item : String)-> Bool{
        var request = NSMutableURLRequest(URL: NSURL(string: url)!)
        
        
   //     "http://54.173.88.148:8081/restaurants/carnitas-snack-shack-san-diego/"
        
        var session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        var params = ["pizza" : pizza,
                      "picture": picture,
                      "rating" : rating,
                      "price" : price,
                      "item" : item ] as NSDictionary
        

        var err = NSError?()
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        var task = session.dataTaskWithRequest(request, completionHandler: { data, response, error -> Void in
            println("Response : \(response)")
            
            var strData = NSString(data : data, encoding: NSUTF8StringEncoding)
            println("Body: \(strData)")
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves, error: &err)
            
            if(err != nil){
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                
            } else {
                if let parseJSON = json {
                    var success = parseJSON["success"] as? Int
                    println("Success: \(success)")
                } else {
                    let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                    println("Error could not parse JSON: \(jsonStr)")
                }
            }
            
        })
        

        task.resume()
        
        return true
    }
    
    
}