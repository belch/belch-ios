//
//  SearchViewController.swift
//  BelchApp
//
//  Created by Matthew Hogetvedt on 11/25/14.
//  Copyright (c) 2014 Belch. All rights reserved.
//  Edite by Rumzi Khan on 11/28/14
//

import UIKit

class RestaurantCell: UITableViewCell {
    @IBOutlet var RestaurantLabel: UILabel!
    @IBOutlet var PhoneLabel: UILabel!
    @IBOutlet var ReviewLabel: UILabel!
    @IBOutlet var RestaurantImage: UIImageView!
    @IBOutlet var BelchRatingImage: UIImageView!
}

class SearchViewController: GAITrackedViewController, NSURLConnectionDelegate, UISearchBarDelegate, UISearchDisplayDelegate {
    @IBOutlet weak var reviewTable: UITableView!
    @IBOutlet weak var loadMoreButton: UIButton!
    
    var restaurantId = "carnitas-snack-shack-san-diego" // TODO this gets passed in from the restaurant list
    var service: RestaurantService!
    var restToPass: Restaurant?
    var restaurants = [Restaurant]()    //holds all the restaurants in an arrow
    var filteredRestaurants = [Restaurant]()
    var belchReviews = [String:Int]() //holds the belch rating for each restaurant name
    var belchReviewCounts = [String:Int]() //holds the belch review count for each restaurant
    var belchPrices = [String:Int]() //holds the belch pricing for each restaurant name
    var slicesPassedIn: Int?    //holds the slices the user picked
    
    
    @IBOutlet weak var restaurantTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "belch_logo.png")
        let restaurantLogo = UIImageView(image: logo)
        //  restaurantLogo.set
        self.navigationItem.titleView = restaurantLogo
        service = RestaurantService()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.setToolbarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        for p in 1...2 {
            let url = NSURL(string: "http://54.173.88.148:8081/restaurants/location/San%20Diego?p=\(p)&pizza=\(slicesPassedIn!)")
            var request = NSURLRequest(URL: url!)
            var data = NSURLConnection.sendSynchronousRequest(request, returningResponse: nil, error: nil)
            if data != nil {
                var json = JSON(data: data!)
                for i in 0...9 {
                    var id = json[i]["id"].stringValue
                    var name = json[i]["name"].stringValue
                    var phone = json[i]["phone"].stringValue
                    var image = json[i]["image"].stringValue
                    var rating = json[i]["yelp_rating"].intValue
                    var ratingImg = json[i]["rating_img_url"].stringValue
                    
                    //Calculate belch and price rating
                    var reviews = json[i]["reviews"]
                    self.belchReviewCounts[name] = reviews.count
                    
                    if reviews.count > 0 {
                        var reviewCount = reviews.count
                        var cost:Float = 0.0
                        var sum:Float = 0.0
                        for c in 0...reviewCount {
                            var belch = reviews[c]["rating"].intValue
                            var money = reviews[c]["price"].intValue
                            cost = cost + Float(money)
                            sum = sum + Float(belch)
                        }
                        //println("Restaurant: \(name)")
                        var avgBelch = Float(sum / Float(reviewCount))
                        var avgCost = Float(cost / Float(reviewCount))
                        //println("avgBelch: \(avgBelch)")
                        //println("avgCost: \(avgCost)")
                        var belchRating: Float = avgBelch / avgCost
                        //println("belchRating: \(belchRating)")
                        var intAvgBelch: Int = Int(round(avgBelch))
                        var intAvgCost: Int = Int(round(avgCost))
                        //println("intAvgBelch: \(intAvgBelch)")
                        //println("intAvgCost: \(intAvgCost)")
                        self.belchReviews[name] = intAvgBelch
                        self.belchPrices[name] = intAvgCost
                        
                        var belchRatingImg = UIImage(named: "burger_4.png")
                        //Get Belch Rating for Restaurant
                        if ( self.belchReviews[name] == 1 ) {
                            var belchRatingImage : UIImage = UIImage(named: "burger_1.png")!
                            belchRatingImg = belchRatingImage
                        } else if ( self.belchReviews[name] == 2 ) {
                            var belchRatingImage : UIImage = UIImage(named: "burger_2.png")!
                            belchRatingImg = belchRatingImage
                        } else if ( self.belchReviews[name] == 3 ) {
                            var belchRatingImage : UIImage = UIImage(named: "burger_3.png")!
                            belchRatingImg = belchRatingImage
                        }
                        var rest = Restaurant(id: id, name: name, phone: phone, image: image, rating: rating, ratingImg: ratingImg, belchRatingImg: belchRatingImg!)
                        self.restaurants.append(rest)
                    } else {
                        var belchRatingImage : UIImage = UIImage(named: "no_belch_review.png")!
                        var belchRatingImg = belchRatingImage
                        var rest = Restaurant(id: id, name: name, phone: phone, image: image, rating: rating, ratingImg: ratingImg, belchRatingImg: belchRatingImg)
                        self.restaurants.append(rest)
                    }
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TableView Code
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // 1
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return self.filteredRestaurants.count
        } else {
            return self.restaurants.count
        }
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientation.Portrait.rawValue
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // 3
        var cell : RestaurantCell? = tableView.dequeueReusableCellWithIdentifier("restaurantCell") as? RestaurantCell
        //dequeueReusableCellWithIdentifier("restaurantCell") as? RestaurantCell
        if cell == nil {
            cell = RestaurantCell()
        }
        
        var rest : Restaurant
        
        if tableView == self.searchDisplayController!.searchResultsTableView {
            cell!.textLabel?.text = filteredRestaurants[indexPath.row].getName()
            
        } else {
            // Get restaurant name and Address
            cell!.RestaurantLabel.text = restaurants[indexPath.row].getName()
            if self.belchReviewCounts[restaurants[indexPath.row].getName()] == 0 {
                cell!.ReviewLabel.text = "Be the first review!"
            } else {
                cell!.ReviewLabel.text = "\(self.belchReviewCounts[restaurants[indexPath.row].getName()]!) Reviews"
            }
            
            //Get Belch Rating for Restaurant
            if ( self.belchReviews[restaurants[indexPath.row].getName()] == 1 ) {
                var belchRatingImage : UIImage = UIImage(named: "burger_1.png")!
                cell!.BelchRatingImage.image = belchRatingImage
            } else if ( self.belchReviews[restaurants[indexPath.row].getName()] == 2 ) {
                var belchRatingImage : UIImage = UIImage(named: "burger_2.png")!
                cell!.BelchRatingImage.image = belchRatingImage
            } else if ( self.belchReviews[restaurants[indexPath.row].getName()] == 3 ) {
                var belchRatingImage : UIImage = UIImage(named: "burger_3.png")!
                cell!.BelchRatingImage.image = belchRatingImage
            } else if ( self.belchReviews[restaurants[indexPath.row].getName()] == 4 ){
                var belchRatingImage : UIImage = UIImage(named: "burger_4.png")!
                cell!.BelchRatingImage.image = belchRatingImage
            } else {
                cell!.BelchRatingImage.image = UIImage(named: "no_belch_review.png")
            }
            
            // Get image data
            var imageURL = restaurants[indexPath.row].getImage()
            var url = NSURL(string:imageURL)
            var image: UIImage?
            var request: NSURLRequest = NSURLRequest(URL: url!)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {(response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                image = UIImage(data: data)
                cell!.RestaurantImage.image = image
                cell!.RestaurantImage.autoresizingMask = UIViewAutoresizing()
                
            })
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.searchDisplayController?.active == true {
            self.performSegueWithIdentifier("searchToRestaurantSegue", sender: tableView)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName = "Restaurant View"
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "searchToRestaurantSegue") {
            let svc = segue.destinationViewController as RestaurantViewController
            if self.searchDisplayController?.active == true {
                let indexPath = self.searchDisplayController!.searchResultsTableView.indexPathForSelectedRow()!
                self.restToPass = self.filteredRestaurants[indexPath.row]
                svc.userPizza = self.slicesPassedIn
                svc.restaurant = self.restToPass
            } else if self.searchDisplayController?.active == false {
                let indexPath = self.restaurantTableView.indexPathForSelectedRow()!
                self.restToPass = self.restaurants[indexPath.row] as Restaurant
                svc.userPizza = self.slicesPassedIn
                svc.restaurant = self.restToPass
            }
        }
    }

    
    // Button action to load more restaurants
    
    // Search Bar Implementation for Search by Term
    func filterContentForSearchText(searchText: String) {
        if searchText.rangeOfString(" ") != nil{
            var toArray = searchText.componentsSeparatedByString(" ")
            var backToString = join("+", toArray)
            let searchText = backToString
            let url = NSURL(string: "http://54.173.88.148:8081/restaurants/location/San%20Diego?q=\(searchText)&pizza=\(slicesPassedIn!)")
            var request = NSURLRequest(URL: url!)
            let queue:NSOperationQueue = NSOperationQueue()
            NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{ response, data, error in
                if data != nil {
                    self.filteredRestaurants = [Restaurant]()
                    var json = JSON(data: data!)
                    if json.count > 0 {
                        for i in 0...json.count-1 {
                            var id = json[i]["id"].stringValue
                            var name = json[i]["name"].stringValue
                            var phone = json[i]["phone"].stringValue
                            var image = json[i]["image"].stringValue
                            var rating = json[i]["yelp_rating"].intValue
                            var ratingImg = json[i]["rating_img_url"].stringValue
                            //Calculate belch and price rating
                            var reviews = json[i]["reviews"]
                            self.belchReviewCounts[name] = reviews.count
                            if reviews.count > 0 {
                                var reviewCount = reviews.count
                                var cost:Float = 0.0
                                var sum:Float = 0.0
                                for c in 0...reviewCount {
                                    var belch = reviews[c]["rating"].intValue
                                    var money = reviews[c]["price"].intValue
                                    cost = cost + Float(money)
                                    sum = sum + Float(belch)
                                }
                                
                                var avgBelch = Float(sum / Float(reviewCount))
                                var avgCost = Float(cost / Float(reviewCount))
                                
                                var belchRating: Float = avgBelch / avgCost
                                
                                var intAvgBelch: Int = Int(round(avgBelch))
                                var intAvgCost: Int = Int(round(avgCost))
                                
                                self.belchReviews[name] = intAvgBelch
                                self.belchPrices[name] = intAvgCost
                                
                                var belchRatingImg = UIImage(named: "burger_4.png")
                                //Get Belch Rating for Restaurant
                                if ( self.belchReviews[name] == 1 ) {
                                    var belchRatingImage : UIImage = UIImage(named: "burger_1.png")!
                                    belchRatingImg = belchRatingImage
                                } else if ( self.belchReviews[name] == 2 ) {
                                    var belchRatingImage : UIImage = UIImage(named: "burger_2.png")!
                                    belchRatingImg = belchRatingImage
                                } else if ( self.belchReviews[name] == 3 ) {
                                    var belchRatingImage : UIImage = UIImage(named: "burger_3.png")!
                                    belchRatingImg = belchRatingImage
                                }
                                var rest = Restaurant(id: id, name: name, phone: phone, image: image, rating: rating, ratingImg: ratingImg, belchRatingImg: belchRatingImg!)
                                self.filteredRestaurants.append(rest)
                            } else {
                                var belchRatingImage : UIImage = UIImage(named: "no_belch_review.png")!
                                var belchRatingImg = belchRatingImage
                                var rest = Restaurant(id: id, name: name, phone: phone, image: image, rating: rating, ratingImg: ratingImg, belchRatingImg: belchRatingImg)
                                self.filteredRestaurants.append(rest)
                            }
                            
                            //self.searchDisplayController?.searchResultsTableView.reloadData()
                        }
                    }
                    self.searchDisplayController?.searchResultsTableView.reloadData()
                }
            })
        } else {
            let url = NSURL(string: "http://54.173.88.148:8081/restaurants/location/San%20Diego?q=\(searchText)&pizza=\(slicesPassedIn!)")
            var request = NSURLRequest(URL: url!)
            let queue:NSOperationQueue = NSOperationQueue()
            NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{ response, data, error in
                if data != nil {
                    self.filteredRestaurants = [Restaurant]()
                    var json = JSON(data: data!)
                    if json.count > 0 {
                        for i in 0...json.count-1 {
                            var id = json[i]["id"].stringValue
                            var name = json[i]["name"].stringValue
                            var phone = json[i]["phone"].stringValue
                            var image = json[i]["image"].stringValue
                            var rating = json[i]["yelp_rating"].intValue
                            var ratingImg = json[i]["rating_img_url"].stringValue
                            //Calculate belch and price rating
                            var reviews = json[i]["reviews"]
                            self.belchReviewCounts[name] = reviews.count
                            if reviews.count > 0 {
                                var reviewCount = reviews.count
                                var cost:Float = 0.0
                                var sum:Float = 0.0
                                for c in 0...reviewCount {
                                    var belch = reviews[c]["rating"].intValue
                                    var money = reviews[c]["price"].intValue
                                    cost = cost + Float(money)
                                    sum = sum + Float(belch)
                                }
                                
                                var avgBelch = Float(sum / Float(reviewCount))
                                var avgCost = Float(cost / Float(reviewCount))
                                
                                var belchRating: Float = avgBelch / avgCost
                                
                                var intAvgBelch: Int = Int(round(avgBelch))
                                var intAvgCost: Int = Int(round(avgCost))
                                
                                self.belchReviews[name] = intAvgBelch
                                self.belchPrices[name] = intAvgCost
                                
                                var belchRatingImg = UIImage(named: "burger_4.png")
                                //Get Belch Rating for Restaurant
                                if ( self.belchReviews[name] == 1 ) {
                                    var belchRatingImage : UIImage = UIImage(named: "burger_1.png")!
                                    belchRatingImg = belchRatingImage
                                } else if ( self.belchReviews[name] == 2 ) {
                                    var belchRatingImage : UIImage = UIImage(named: "burger_2.png")!
                                    belchRatingImg = belchRatingImage
                                } else if ( self.belchReviews[name] == 3 ) {
                                    var belchRatingImage : UIImage = UIImage(named: "burger_3.png")!
                                    belchRatingImg = belchRatingImage
                                }
                                var rest = Restaurant(id: id, name: name, phone: phone, image: image, rating: rating, ratingImg: ratingImg, belchRatingImg: belchRatingImg!)
                                self.filteredRestaurants.append(rest)
                            } else {
                                var belchRatingImage : UIImage = UIImage(named: "no_belch_review.png")!
                                var belchRatingImg = belchRatingImage
                                var rest = Restaurant(id: id, name: name, phone: phone, image: image, rating: rating, ratingImg: ratingImg, belchRatingImg: belchRatingImg)
                                self.filteredRestaurants.append(rest)
                            }
                            
                            //self.searchDisplayController?.searchResultsTableView.reloadData()
                        }
                    }
                    self.searchDisplayController?.searchResultsTableView.reloadData()
                }
            })
        }
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        self.filterContentForSearchText(searchString)
        return true
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        self.filterContentForSearchText(self.searchDisplayController!.searchBar.text)
        return true
    }
    
    func searchBarSearchButtonClicked( searchBar: UISearchBar!) {
        self.searchDisplayController?.searchResultsTableView.reloadData()
    }
    // Button action to load more restaurants
    
    
}


