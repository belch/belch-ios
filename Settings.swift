//
//  Settings.swift
//  BelchApp
//
//  Created by Matthew Hogetvedt on 11/20/14.
//  Copyright (c) 2014 Belch. All rights reserved.
//

import Foundation

class Settings {
    var root: String = "http://54.173.88.148:8081"
    var viewRestaurants : String = "http://54.173.88.148:8081/restaurants/"
    
    // Returns a specific restaurant
    func getRestaurant(r : String) -> String {
        return viewRestaurants + r + "/"
    }
}