//
//  ReviewViewController.swift
//  BelchApp
//
//  Created by Matthew Hogetvedt on 11/29/14.
//  Copyright (c) 2014 Belch. All rights reserved.
//

import UIKit

class ReviewViewController: GAITrackedViewController {
    @IBOutlet weak var reviewImage: UIImageView!
    @IBOutlet weak var reviewFoodType: UILabel!
    @IBOutlet weak var reviewRating: UILabel!
    @IBOutlet weak var reviewPrice: UILabel!
//    @IBOutlet weak var reviewPizza: UILabel!

    
    var imgFactory: ImageFactory!
    
    var review: Review!

    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "belch_logo.png")
        let restaurantLogo = UIImageView(image: logo)
        //  restaurantLogo.set
        self.navigationItem.titleView = restaurantLogo
        imgFactory = ImageFactory()
     //   review = Review() // This is the variable to be initialized from passed in review
        
        if(self.review.getImage() != ""){
            reviewImage.image = self.imgFactory.createUIImageFromString(review.getImage())
        } else {
            //  implement place holder image
            reviewImage.image = UIImage(named: "no_image.jpg")
        }
        reviewFoodType.text = self.review.getItem()
        reviewRating.text = String(self.review.getRating())
        reviewPrice.text = String(self.review.getPrice())
     //   reviewPizza.text = String(self.review.getPizza())
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.screenName = "Review view"
    }
}
