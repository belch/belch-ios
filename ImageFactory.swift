//
//  ImageFactory.swift
//  BelchApp
//
//  Created by Matthew Hogetvedt on 11/29/14.
//  Copyright (c) 2014 Belch. All rights reserved.
//

import UIKit
import Foundation


class ImageFactory{
    
    func createUIImageFromString(pic : String)->UIImage{
        var url: NSURL? = NSURL(string: pic)
        var err: NSError?
        var image : UIImage?
        
        var data: NSData? = NSData(contentsOfURL: url!, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)
        
        image = UIImage(data: data!)
        
        return image!
    }
    
}